
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greta Pizza</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php
session_start();
include 'menu.php';
include 'php/utilitaire.php';
include 'php/bdd/bddPizza.php';
if(isset($_POST['nom'])&& isset($_POST['email'])&& isset($_POST['adresse'])&&isset($_POST['mdp1'])&&isset($_POST['mdp2']))
{
    echo" Ok"; 
    $nom=test_input($_POST['nom']);
    $email=test_input($_POST['email']);
    $adresse=test_input($_POST['adresse']);
    $mdp1=test_input($_POST['mdp1']);
    $mdp2=test_input($_POST['mdp2']);
    if($mdp1==$mdp2 && strlen($mdp1)>=6)
    {
        $mdp=sha1($mdp1);
        echo" Ok mdp"; 
        echo $mdp;
        createClient($nom,$adresse,$email,$mdp);
        header("location:index.php");
    }
    else{
        echo "<b class='text-danger'>Ereur MDP</b>";
    }
}
?>
<div class="container ">
<h1>Inscription</h1>
<form action="inscription.php" method="POST">
<label for="inputNom">Nom :</label>
<input type="text" id="inputNom" min="1" name="nom">

<label for="inputEmail">Email :</label>
<input type="email" id="inputEmail" name="email">

<label for="inputAdresse">Adresse :</label>
<input type="text" id="inputAdresse" name="adresse" min="2">

<label for="inputMdp1">Mdp :</label>
<input type="password" id="inputMdp1" name="mdp1" min="6">

<label for="inputMdp2">Mdp :</label>
<input type="password" id="inputMdp2" name="mdp2" min="6">

<button>Envoyer</button>
</form>
</div>
<?php include 'footer.php';?>
<?php include 'scripts.php'; ?>
</body>

</html>

