let laPizza;
let divTailles=document.querySelector("#tailles");
let divNombre=document.querySelector("#nombre");

async function fetchPizza(idPizza)
{
    
    let reponse = await fetch("php/pizzas.php?idPizza="+idPizza);
    
    //  console.log(await reponse.text());
    let pizza=await reponse.json();
     console.log(await pizza)
     laPizza=await pizza;
    return  pizza;
}

async function fetchTailles()
{
    let reponse=await fetch("php/tailles.php");
    let arrayTailles=await reponse.json();
    // console.log(await arrayTailles);
    return  arrayTailles;
}
async function fetchPrix()
{
    let tarrif=laPizza.idTarif;
    let taille=document.querySelector("#tailles").value;
    console.log(tarrif+" "+taille);
    let reponse=fetch("php/prix.php?taille="+taille+"&tarif="+tarrif);
    let prix= (await reponse).json();
    console.log(await prix);
    return  prix;
}

async function affichePizza()
{
   
    let pizza=await fetchPizza(idPizza);
    let divMain=document.querySelector("#main");
    let chaineHTML="";
    divMain.innerHTML+="<h3>"+pizza.nomPizza+"</h3><br>Ingredients :<br><ul>";
    for (const ingredient of pizza.ingredients) {
        chaineHTML+="<li>"+ingredient.nomIngredient+"</li>";
    }
    divMain.innerHTML+="</ul>"
    if(pizza.isNew==1)
    {
        chaineHTML+="<br>New <br>"
    }
    if(pizza.isVegetarienne==1)
    {
        chaineHTML+="<br>Vegetarienne <br>"
    }
    divMain.innerHTML+=chaineHTML;
}

async function afficheTaille()
{
    
    let tailles=await fetchTailles();;
    for (const taille of await tailles) {
        divTailles.innerHTML+= `<option value="${taille[0]}">${taille[1]}</option>`
    }
}

async function affichePrix()
{
    let divPrix=document.querySelector("#prix");
    let nombre=document.querySelector("#nombre").value
    let prixUnit= await fetchPrix();
    let prixTotal=parseFloat(await prixUnit)*nombre;
    prixTotal="Prix = "+prixTotal.toFixed(2)+" €";
    divPrix.innerHTML=prixTotal;
}

window.addEventListener("load", async function(){
    await afficheTaille();
    await affichePizza();
    await affichePrix();
})
    



divTailles.addEventListener("change",affichePrix);
divNombre.addEventListener("change",affichePrix);



