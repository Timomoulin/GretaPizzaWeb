

async function fetchPanier()
{
    
    let reponse = await fetch("php/ligneCommande.php");
    
    //  console.log(await reponse.text());
    let panier=await reponse.json();
     console.log(await panier)
    return await panier;
}
async function fetchTailles()
{
    let reponse=await fetch("php/tailles.php");
    let arrayTailles=await reponse.json();
    // console.log(await arrayTailles);
    return arrayTailles;
}

async function affichePanier()
{
    let panier=await fetchPanier();
    let lesTailles=await fetchTailles();
    let tbody=document.querySelector("#panier");
    let chaineHTML="";
    for (let i=0;i<panier.length;i++) {
        let lignePanier=panier[i];
        let nomPizza=lignePanier.pizza.nomPizza;
        let idTaille=lignePanier.taille;
        let laTaille;
       
        let quantite=lignePanier.quantite;
        let montant=lignePanier.montant; //TODO
        chaineHTML +=`<tr>`;
        chaineHTML += `<th scope="row">${i+1}</th>`;
        chaineHTML +=`<td>${nomPizza}</td>`;
        chaineHTML +=`<td><select>`; //a mettre en chaine
        for (const uneTaille of lesTailles) {
           
            if(uneTaille[0]==idTaille)
            {
                chaineHTML+=`<option selected>${uneTaille[1]}</option>`;
                laTaille=uneTaille[1];
            }
            else{
            chaineHTML+=`<option>${uneTaille[1]}</option>`;
            }
        } 
        chaineHTML +=`</select></td>`
        chaineHTML +=`<td><input type="number" min="1" size="2" name="taille" value="${quantite}"></td>`;
        chaineHTML +=`<td>${montant}</td>`;
        chaineHTML +=`</tr>`;
    }
    tbody.innerHTML=chaineHTML;
}

affichePanier();