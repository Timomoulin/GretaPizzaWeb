// let lesPizzas;
let inputRecherche = document.querySelector("#recherchePizza");
let dataList = document.querySelector("#rechercheFiltre");


async function fetchPizzas() {
    let reponse = await fetch("php/pizzas.php");
    let data = await reponse.json();
    console.log(data);
    return data;
}

async function fetchPizzasByGenre(nomGenre) {
    let reponse = await fetch("./php/pizzas.php?nomGenre=" + nomGenre);
    let data = await reponse.json();
    console.log(pizzas);
    return data;
}

function affichePizzas(lesPizzas) {
    let divClassics = document.querySelector("#pizzasClassics");
    let divIta = document.querySelector("#pizzasItaliennes");
    console.log(divIta);
    divClassics.innerHTML = `<button class="btn btn-secondary" type="button" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>  Chargement...</button>`;
    divIta.innerHTML = `<button class="btn btn-secondary" type="button" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>  Chargement...</button>`;
    let chaineHTMLClassics = ""
    let chaineHTMLItaliennes = "";
    for (let pizza of lesPizzas) {
        if (pizza.nomGenre == "Nos classics") {
            chaineHTMLClassics += `<div class='col-sm-1 col-0'></div> `;
            chaineHTMLClassics += `<div  id="${pizza.id}"  class="card bg-white text-dark col-md-2 col-sm-4 col-10 pizza"><a href="consulterPizza.php?idPizza=${pizza.id}"><h5 class="card-title fluid">${pizza.nomPizza}</h5><img class="card-img" src="./img/pizzaDefaut.jpg" alt="Card image"><div class="card-img-overlay card-link" ><p class="card-text"></p></div></a></div>`
            chaineHTMLClassics += "<div class='col-sm-1 col-0'></div>";
        } else {

            chaineHTMLItaliennes += `<div class='col-sm-1 col-0'></div> `;
            chaineHTMLItaliennes += `<div  id="${pizza.id}"  class="card bg-white text-dark col-md-2 col-sm-4 col-10 pizza"><a href="consulterPizza.php?idPizza=${pizza.id}"><h5 class="card-title fluid">${pizza.nomPizza}</h5><img class="card-img" src="./img/pizzaDefaut.jpg" alt="Card image"><div class="card-img-overlay card-link" ><p class="card-text"></p></div></a></div>`
            chaineHTMLItaliennes += "<div class='col-sm-1 col-0'></div>";
        }
    }
    divClassics.innerHTML = chaineHTMLClassics;
    divIta.innerHTML = chaineHTMLItaliennes;

}

function afficheDataList(lesPizzas) {
    let chaineHTML = "";
    for (let unePizza of lesPizzas) {
        chaineHTML += `<option id="${unePizza.id}">${unePizza.nomPizza}</option>`
    }
    dataList.innerHTML = chaineHTML;
}

fetchPizzas().then(function (resultatDeFetchPizzas) {
    affichePizzas(resultatDeFetchPizzas)
});
fetchPizzas().then(function (resultat) {
    afficheDataList(resultat)
});

inputRecherche.addEventListener("keyup", async function () {
    let nomRechercher = trim(inputRecherche.value);
    if (!!nomRechercher) {
        console.log(inputRecherche.value);

        let lesPizzas = await fetchPizzas();
        let lesPizzasRechercher = [];
        for (const unePizza of lesPizzas) {
            if (unePizza.nomPizza.toLowerCase().includes(nomRechercher.toLowerCase())) {
                lesPizzasRechercher.push(unePizza);
            }
        }
        affichePizzas(lesPizzasRechercher);
    } else {
        fetchPizzas().then(function (resultatDeFetchPizzas) {
            affichePizzas(resultatDeFetchPizzas)
        });
    }
})