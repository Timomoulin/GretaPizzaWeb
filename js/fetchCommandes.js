async function fetchCommandes()
{
    let reponse = await fetch("php/commandes.php");
    // console.log(reponse.json());
    let commandes=await reponse.json();
    console.log(commandes)
    return commandes;
}

async function afficheCommandes()
{
    let tableCommandes=document.querySelector("#commandes");
    let lesCommandes=await fetchCommandes();
    let chaineHTML="";
    let i=0;
    for (const laCommande of lesCommandes) {
        i+=1;
        chaineHTML+="<tr>";
        chaineHTML+="<td>"+i+"</td>";
        chaineHTML+="<td>"+laCommande.dateHeure+"</td>";
        if(laCommande.aLivrer=="1"){
            chaineHTML+="<td>Livraison</td>"
        }
        else{
            chaineHTML+="<td>Sur place/a emporter</td>"
        }
        if(laCommande.fini=="1")
        {
            chaineHTML+="<td>La commande a été livrée</td>";
        }
        else{
            chaineHTML+="<td>La commande est en cours de préparation</td>";
        }
        
        chaineHTML+="<td>"+laCommande.montant+"</td>";
        chaineHTML+="</tr>";
    }
    tableCommandes.innerHTML=chaineHTML;
}
afficheCommandes();

