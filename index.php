
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greta Pizza</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php
session_start();
include 'menu.php';

?>
<div class="container">
    <div class="row d-flex justify-content-center">
    <div class="input-group input-group-lg col-md-4">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroup-sizing-lg"> Recherche </span>
  </div>
  <input list="rechercheFiltre" id="recherchePizza" type="search" class="form-control" aria-label="Recherche" aria-describedby="inputGroup-sizing-sm">
  <datalist id="rechercheFiltre">
  <label for="">Pizzas : </label>
    <option>Exemple (Pizza)</option>
  <option>Exemple (Ingredient)</option>  
  </datalist>
</div>
    </div>
    <div class="row">   
        <h2>Nos classics :</h2>
    </div>
    <div class="row d-flex justify-content-around" id="pizzasClassics">
    </div>

    <div class="row"> 
        <h2>Nos pizzas Italiennes :</h2>
    </div>
    <div class="row d-flex justify-content-around" id="pizzasItaliennes">
    </div>
    
</div>

<?php include 'footer.php';?>
<script src="js/fetchPizzas.js"></script>
<?php include 'scripts.php'; ?>
</body>
</html>

