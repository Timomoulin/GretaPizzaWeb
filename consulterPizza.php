
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greta Pizza</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php
session_start();
include 'menu.php';
include 'php/utilitaire.php';

if( isset($_GET["idPizza"]))
{
$idPizza=test_input($_GET["idPizza"]);
}

?>
<div class="container d-flex flex-column justify-content-center">
    <div class="row justify-content-center">
        
        <div id="main" class=" col-sm-4 "></div>
        
    </div>    
     <div class="row d-flex justify-content-center">
    <form action="./php/ligneCommande.php" method="post" id="commandes">

        <div class="col-10 input-group">
            <div class="input-group-prepend">
                <label class="input-group-text" for="tailles">Taille : </label>
            </div>
            <select name="taille" id="tailles" class="custom-select mr-sm-2" id="inlineFormCustomSelect">

            </select>
        </div>
        <br>
        <div class="input-group col-10">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Nombre :</span>
            </div>
            <input type="number" id="nombre"  name="nombre" class="form-control" value=1 min=1 max=10 aria-label="Nombre"
                aria-describedby="basic-addon1">   
        </div>
        <br>
            <div class="row col-10"> 
                <div id="prix"></div>
            </div>  
        <br>
       
        <input type="hidden" name="idPizza" value="<?php echo $idPizza;?>">
        <?php if( isset($_SESSION['Id']) )
        {
        echo "<button class='btn '>Ajouter au panier</button>";
        }
        else{
            echo "<a href='identification.php' class='btn btn-info' role='button'>Se connecter</a>";
        }
        ?>
       
    </form>
    </div>
</div>
<?php include 'footer.php';?>
<script>let idPizza= <?php echo $idPizza;?> </script>
<script src="js/fetchPizza.js"></script>
<?php include 'scripts.php'; ?>
</body>

</html>

