
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greta Pizza</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php
session_start();
include 'menu.php';
include 'php/utilitaire.php';
if( empty($_SESSION['Id']) )
{
    header("Location: identification.php");
    exit;
}


?>
<div class="container ">
    <div class="row">
        <h1>Votre Panier :</h1>
    </div>
    <div class="row d-flex justify-content-center table-responsive">
   <table class="table table-bordered col-md-10 ">
   <thead>
    <tr>
    <th scope="col">#</th>
      <th scope="col">Pizza</th>
      <th scope="col">Taille</th>
      <th scope="col">Quantité</th>
      <th scope="col">Montant</th>
    </tr>
  </thead>
  <tbody id="panier">
  </tbody>
   </table>
</div>
   <form action="php/commandes.php" method="POST">
   <div class="form-check">
            <input class="form-check-input" type="radio" value="1" name="livraison" id="exampleRadios1" value="a"
                checked>
            <label class="form-check-label" for="exampleRadios1">
                Livraison
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" value="0" name="livraison" id="exampleRadios2" value="b">
            <label class="form-check-label" for="exampleRadios2">
                Sur place/A Emporter
            </label>
        </div>
    </div>
    <button class='btn '>Confirmer la Commande</button>
</form>
</div>
<?php include 'footer.php';?>
<script src="js/fetchPanier.js"></script>
<?php include 'scripts.php'; ?>
</body>

</html>

