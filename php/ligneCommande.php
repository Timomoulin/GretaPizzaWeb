<?php

include 'bdd/bddPizza.php';
include 'utilitaire.php';
header("Content-type:application/json");

session_start();
if( isset($_SESSION['Id']) )
{
    
    if($_SERVER["REQUEST_METHOD"] == "POST" and isset($_POST["idPizza"]) and isset($_POST["taille"]) and isset($_POST["nombre"]) )
        {    
            $idPizza=test_input($_POST["idPizza"]);
            $taille=test_input($_POST["taille"]);
            $nombre=test_input($_POST["nombre"]);
            $pizza= getPizzaById($idPizza);
            $lignePanier=new lignePanier($pizza,$taille,$nombre);
            $prixUnitaire=getPrixByTailleAndTarif($taille,$pizza->getIdTarif());
            $prixUnitaire=tofloat($prixUnitaire);
            $lignePanier->setMontant($prixUnitaire*$nombre);
            
            if(isset($_SESSION['panier']))
            {
                $lignePanier->setId(count($_SESSION['panier']));
                array_push($_SESSION['panier'],$lignePanier);
            }
            else{
                $panier=array();
                $lignePanier->setId(0);
                array_push($panier,$lignePanier);
                $_SESSION['panier']=$panier;
            }
            header("location: /../GretaPizzaWeb/panier.php");
            }
            
        
    
    elseif($_SERVER["REQUEST_METHOD"] == "GET")
    {
     
        $panier=$_SESSION['panier'];
        if (isset($panier))
        {
            $jsonPanier=json_encode($panier);
            echo $jsonPanier;
        }
        else{
            $pasDePanier=json_encode("Pas de panier");
            echo $pasDePanier;
        }   
    }
    
}
    else{
        header("Location: identification.php");
        exit;
    }
   

    ?>