<?php

include 'bdd/bddPizza.php';
include 'utilitaire.php';
header("Content-type:application/json");
session_start();
if(isset($_SESSION['Id']))
{
    $idClient=$_SESSION['Id'];

  if($_SERVER["REQUEST_METHOD"] == "GET")
  {
  $lesCommandes=getCommandesByIdClient($idClient);
  echo $lesCommandes;
  }
  elseif($_SERVER["REQUEST_METHOD"] == "POST")
  {
      if(isset($_SESSION["panier"]) and isset($_POST['livraison']) and count($_SESSION["panier"])>0 )
      {
          $livraison=test_input($_POST['livraison']);
          $date=date("Y-m-d h:i:s",strtotime('+3 hours'));
          //Todo Ajouter X heures 
          $panier=new Commande($idClient,$date,$livraison);
          //$panier->setLignesDuPanier($_SESSION["panier")
          $arrayLignePanier=$_SESSION["panier"];
          $lesLignesDuPanier=array();
          $montantTotal=0.0;
          foreach($arrayLignePanier as $ligneCommande)
          {
            $pizza=$ligneCommande->getPizza();
            $pizza= getPizzaById($pizza->getId());
            $prixUnitaire=getPrixByTailleAndTarif($ligneCommande->getTaille(),$pizza->getIdTarif());
            $prixUnitaire=tofloat($prixUnitaire);
            $ligneCommande->setMontant($prixUnitaire*$ligneCommande->getQuantite());
            $montantTotal+=$ligneCommande->getMontant();
            array_push($lesLignesDuPanier,$ligneCommande);
          }
          $panier->setMontant($montantTotal);
          $panier->setLignesDuPanier($lesLignesDuPanier);
          
        $resultat=createCommande($panier);
        if($resultat==true){
          unset($_SESSION["panier"]);
          header("location: /../GretaPizzaWeb/panier.php");
        }
      }
  }
}
  ?>