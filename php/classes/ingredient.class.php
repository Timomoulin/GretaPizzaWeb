<?php
class Ingredient implements \JsonSerializable
{
     private $id;
     private $nomIngredient;

     public function __construct($id, $nom){
        $this->id = $id;
        $this->nomIngredient = $nom;
    }

     public function getId(){
        return $this->id;
    }
    public function getNomIngredient(){
        return $this->nomIngredient;
    }
    public function setId($new_id){
        $this->id = $new_id;
    }
    public function setIngredient($new_nom){
        $this->nomIngredient = $new_nom;
    } 

    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
?>