<?php
class Commande implements \JsonSerializable
{
     private $id;
     private $idClient;
     private $dateHeure;
     private $aLivrer;
     private $fini;
     private $montant;
     private $lignesDuPanier= array(); //comprendre

     public function __construct( $idClient,$dateHeure,$aLivrer){
        
        $this->idClient = $idClient;
        $this->dateHeure = $dateHeure;
        $this->aLivrer = $aLivrer;
        //date("Y-m-d h:i:s")
    }

     public function getId(){
        return $this->id;
    }
    public function getIdClient(){
        return $this->idClient;
    }
    public function getDateHeure(){
        return $this->dateHeure;
    }
    public function getALivrer(){
        return $this->aLivrer;
    }
    public function getFini(){
        return $this->fini;
    }
    public function getMontant(){
        return $this->montant;
    }
    public function getLignesDuPanier(){
        return $this->lignesDuPanier;
    }
    public function setId($new_id){
        $this->id = $new_id;
    }
    public function setIdClient($newIdClient){
        $this->idClient = $newIdClient;
    } 
    public function setDateHeure($newDate){
        $this->dateHeure = $newdate;
    }
    public function setALivrer($newLivrer){
        $this->aLivrer = $newLivrer;
    }
    public function setFini($newFini){
        $this->fini = $newFini;
    }
    public function setMontant($newMontant){
        $this->montant = $newMontant;
    }
    public function setLignesDuPanier($newLignes){
        $this->lignesDuPanier = $newLignes;
    }
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
?>