<?php
class LignePanier implements \JsonSerializable
{
    private  $id;
     private $pizza;
     private $taille;
     private $quantite;
     private $montant;

     public function __construct($unePizza, $uneTaille,$uneQuantite){
        $this->pizza = $unePizza;
        $this->taille = $uneTaille;
        $this->quantite = $uneQuantite;
    }
    public function getId(){
        return $this->id;
    }
     public function getPizza(){
        return $this->pizza;
    }
    public function getTaille(){
        return $this->taille;
    }
    public function getQuantite(){
        return $this->quantite;
    }
    public function getMontant(){
        return $this->montant;
    }
    public function setId($newId){
        $this->id = $newId;
    }
    public function setPizza($newPizza){
        $this->pizza = $newPizza;
    }
    public function setTaille($newTaille){
        $this->taille = $newTaille;
    } 
    public function setQuantite($newQuantite){
        $this->quantite = $newQuantite;
    } 
    public function setMontant($newMontant){
        $this->montant = $newMontant;
    }
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
?>