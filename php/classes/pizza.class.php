<?php
class Pizza implements \JsonSerializable
{
    private $id;
    private $nomPizza;
    private $isNew;
    private $isVegetarienne;
    private $idTarif;
    private $nomGenre;
    private $ingredients= array();

    public function __construct($id, $nom,$estNew,$estVegetarienne,$tarif,$genre){
        $this->id = $id;
        $this->nomPizza = $nom;
        $this->isNew=$estNew;
        $this->isVegetarienne=$estVegetarienne;
        $this->idTarif=$tarif;
        $this->nomGenre=$genre;
    }

    // Les Getters
    public function getId(){
        return $this->id;
    }
    public function getNomPizza(){
        return $this->nomPizza;
    }
    public function getIsNew(){
        return $this->isNew;
    }
    public function getIsVegetarienne(){
        return $this->isVegetarienne;
    }
    public function getIdTarif(){
        return $this->idTarif;
    }
    public function getNomGenre(){
        return $this->nomGenre;
    }
    public function getIngredients(){
        return $this->ingredients;
    }
    //Fin des Getters

    //Les setters
    public function setId($new_id){
        $this->id = $new_id;
    }
    public function setNomPizza($new_nom){
        $this->nomPizza = $new_nom;
    }
    public function setIsNew($new_boolean){
        $this->isNew = $new_boolean;
    }
    public function setIdTarif($new_idTarif){
        $this->idTarif = $new_idTarif;
    }
    public function setNomGenre($new_NomGenre){
        $this->idGenre = $new_NomGenre;
    }
    public function setIngredients($lesIngredients){
        $this->ingredients = $lesIngredients;
    }
    //Fin des setters
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
    public function avoirIngredient($unIngredient)
    {
        foreach($this->ingredients as $ingredient)
        {
            if($ingredient->getId()==$unIngredient->getId())
            {
                return true;
            }
        }
        return false;
    }

}
?>