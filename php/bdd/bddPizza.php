<?php

require  __DIR__.'/../classes/pizza.class.php';
require __DIR__.'/../classes/ingredient.class.php';
require  __DIR__.'/../classes/lignePanier.class.php';
require __DIR__.'/../classes/commande.class.php';


function getAllPizzas()
{
  include 'base.php'; 
try {
    $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT numPizza,nomPizza,new,vegetarienne,numTarif,nomGenre  FROM pizzas inner join genres on pizzas.numGenre=genres.numGenre ");
    $stmt->execute();
  
    $resultat = ($stmt->fetchAll());
    $lesPizzas= array();
    foreach($resultat as $pizza)
    {
        $unePizza=new Pizza($pizza[0],$pizza[1],$pizza[2],$pizza[3],$pizza[4],$pizza[5]);
        $lesIngredients=getAllIngredientsByPizza($unePizza);
        $unePizza->setIngredients($lesIngredients);
        array_push($lesPizzas,$unePizza);
    }
    return $lesPizzas;
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}

function getAllPizzasByGenre($nomGenre)
{
  include 'base.php';
try {
    $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT numPizza,nomPizza,new,vegetarienne,numTarif,nomGenre  FROM pizzas inner join genres on pizzas.numGenre=genres.numGenre where nomGenre=:nomGenre");
    $stmt->bindParam(':nomGenre', $nomGenre);
    $stmt->execute();
  
    $resultat = ($stmt->fetchAll());
    $lesPizzas= array();
    foreach($resultat as $pizza)
    {
        $unePizza=new Pizza($pizza[0],$pizza[1],$pizza[2],$pizza[3],$pizza[4],$pizza[5]);
        $lesIngredients=getAllIngredientsByPizza($unePizza);
        $unePizza->setIngredients($lesIngredients);
        array_push($lesPizzas,$unePizza);
    }
    return $lesPizzas;
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}

function getPizzaById($idPizza)
{
  include 'base.php';
  try {
      $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("SELECT numPizza,nomPizza,new,vegetarienne,numTarif,nomGenre  FROM pizzas inner join genres on pizzas.numGenre=genres.numGenre where numPizza=:idPizza");
      $stmt ->bindParam(':idPizza',$idPizza);
      $stmt->execute();
    
     
      $resultat = ($stmt->fetchAll());
      $unePizza;
      foreach($resultat as $pizza)
      {
          $unePizza=new Pizza($pizza[0],$pizza[1],$pizza[2],$pizza[3],$pizza[4],$pizza[5]);
          $lesIngredients=getAllIngredientsByPizza($unePizza);
          $unePizza->setIngredients($lesIngredients);
          
      }
    
      return $unePizza;
    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
    $conn = null;
}

function getAllIngredients()
{
  include 'base.php';
  try {
      $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("SELECT * FROM ingredients");
      $stmt->execute();
    
     
      $resultat = ($stmt->fetchAll());
      $lesIngredients= array();
      foreach($resultat as $ingredient)
    {
        $unIngredient=new Ingredient($ingredient[0],$ingredient[1]);
        array_push($lesIngredients,$unIngredient);
    }
    
      return $lesIngredients;
    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
    $conn = null;
}
function getIngredientById($idIngredient)
{
  include 'base.php';
  try {
      $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $stmt = $conn->prepare("SELECT * FROM ingredients where numIngredient=:idIngredient");
      $stmt ->bindParam(':idIngredient',$idIngredient);
      $stmt->execute();
    
     
      $resultat = ($stmt->fetchAll());
      $lesIngredients= array();
      foreach($resultat as $ingredient)
    {
        $unIngredient=new Ingredient($ingredient[0],$ingredient[1]);
        array_push($lesIngredients,$unIngredient);
    }
    
      return $lesIngredients;
    } catch(PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
    $conn = null;
}
function getAllIngredientsByPizza($unePizza)
{
  include 'base.php';
  $idPizza=$unePizza->getId();
  try {
    $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT ingredients.numIngredient, nomIngredient FROM ingredients INNER JOIN composer ON ingredients.numIngredient=composer.numIngredient Where composer.numPizza=:idPizza");
    $stmt ->bindParam(':idPizza',$idPizza);
    $stmt->execute();
  
   
    $resultat = ($stmt->fetchAll());
    $lesIngredients= array();
    foreach($resultat as $ingredient)
  {
      $unIngredient=new Ingredient($ingredient[0],$ingredient[1]);
      array_push($lesIngredients,$unIngredient);
  }
    return $lesIngredients;
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}


function getAllTailles()
{
  include 'base.php';
  try {
    $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT * FROM Tailles ");
    $stmt->execute();
  
   
    $resultat = ($stmt->fetchAll());
    $lesTailles= array();
    foreach($resultat as $taille)
  {
      $uneTaille=array($taille[0],$taille[1],$taille[2],$taille[3]);
      array_push($lesTailles,$uneTaille);
  }
    $lesTailles=json_encode($lesTailles);
    return $lesTailles;
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}

function getPrixByTailleAndTarif($taille,$tarif)
{
  include 'base.php';
  try {
    $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT prix FROM couter where numTarif=:tarif and numTaille=:taille");
    $stmt ->bindParam(':tarif',$tarif);
    $stmt ->bindParam(':taille',$taille);
    $stmt->execute();
  
   
    $resultat = ($stmt->fetchAll());
    $prix;
    foreach($resultat as $resultatPrix)
  {
      $prix=$resultatPrix[0];
  }
    $prix=json_encode($prix);
    return $prix;
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}

function getCommandesByIdClient($idClient)
{
  include 'base.php';
  try {
    $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT * FROM commandes where numClient=:idClient ");
    $stmt ->bindParam(':idClient',$idClient);
    $stmt->execute();
    $resultat = ($stmt->fetchAll());
    $lesCommandes= array();
    foreach($resultat as $commande)
  {
      $uneCommande=new Commande($commande[1],$commande[2],$commande[3]);
      $uneCommande->setId($commande[0]);
      $uneCommande->setFini($commande[4]);
      $uneCommande->setMontant($commande[5]);
      array_push($lesCommandes,$uneCommande);
  }
    $lesCommandes=json_encode($lesCommandes);
    return $lesCommandes;
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}

function createCommande($uneCommande)
{
  include 'base.php';
  try{
  $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  $idClient=$uneCommande->getIdClient();
  $dateLiv=strval($uneCommande->getDateHeure());
  $livraison=$uneCommande->getALivrer();
  $montant=strval($uneCommande->getMontant());
  $lesLignesDeLaCommande=$uneCommande->getLignesDuPanier();
  $stmt = $conn->prepare ("INSERT into commandes (numClient,dateHeureLivraison,àLivrer,montant) Values (:idClient,:dateLiv,:livraison,:montant) ");
  $stmt ->bindParam(':idClient',$idClient);
  $stmt ->bindParam(':dateLiv',$dateLiv);
  $stmt ->bindParam(':livraison',$livraison);
  $stmt ->bindParam(':montant',$montant);
  $stmt->execute();
  $idCommande = $conn->lastInsertId();
  $conn2 = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  $conn2->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $conn2->beginTransaction();
  foreach($lesLignesDeLaCommande as $uneLigne)
  {
    $idPizza=$uneLigne->getPizza()->getId();
    $taille=$uneLigne->getTaille();
    $quantite=$uneLigne->getQuantite();
    $stmt2 = $conn->prepare ("INSERT into comprendre(numCommande,numPizza,numTaille,quantité) Values (:idCommande,:idPizza,:taille,:quantite) ");
    $stmt2 ->bindParam(':idCommande',$idCommande);
    $stmt2 ->bindParam(':idPizza',$idPizza);
    $stmt2 ->bindParam(':taille',$taille);
    $stmt2 ->bindParam(':quantite',$quantite);
    $stmt2->execute();
  }
  $conn2->commit();
  return true;
  
} catch(PDOException $e) {
  $conn2->rollBack();
  echo $sql . "<br>" . $e->getMessage();
  echo $sql2 . "<br>" . $e->getMessage();
  return false;
}
}


function createClient($nomClient,$adresseClient,$emailClient,$mdpClient)
{
  include 'base.php'; 
try {
    $conn = new PDO("mysql:host=$servername;dbname=$myDB", $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("Insert into clients (nomClient,adresseClient,adresseElectronique,mdp) values(:nom,:adr,:email,:mdp)");
    $stmt->bindParam(':nom',$nomClient);
    $stmt->bindParam(':adr',$adresseClient);
    $stmt->bindParam(':email',$emailClient);
    $stmt->bindParam(':mdp',$mdpClient);
    $stmt->execute();
  
    // $resultat = ($stmt->fetchAll());
  
  
    
  } catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
  }
  $conn = null;
}

?>