<?php

// require 'classes/pizza.class.php';
// require 'classes/ingredient.class.php';
include 'bdd/bddPizza.php';
include 'utilitaire.php';
header("Content-type:application/json");

// function test_input($data) {
//     $data= urldecode($data);
//     $data = trim($data);
//     $data = stripslashes($data);
//     $data = htmlspecialchars($data);
//     return $data;
//   }

    if($_SERVER["REQUEST_METHOD"] == "GET" and isset($_GET["nomGenre"]))
    {
     
        $nomGenre=test_input($_GET["nomGenre"]);
        $lesPizzas=getAllPizzasByGenre($nomGenre);
        $jsonPiz=json_encode($lesPizzas);
        echo $jsonPiz;
        
    }
    else if($_SERVER["REQUEST_METHOD"] == "GET" and isset($_GET["idPizza"]))
    {
        $idPizza=test_input($_GET["idPizza"]);
        $laPizza=getPizzaById($idPizza);
        $jsonPiz=json_encode($laPizza);
        // $jsonPiz='{"0":'.$jsonPiz.'}';
        echo $jsonPiz;
    }
    else{
        $lesPizzas=getAllPizzas();
        $jsonPiz=json_encode($lesPizzas);
        echo $jsonPiz;
      
    }
    ?>