<?php

include 'bdd/bddPizza.php';
header("Content-type:application/json");

function test_input($data) {
    $data= urldecode($data);
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }
  if($_SERVER["REQUEST_METHOD"] == "GET" and isset($_GET["tarif"])and isset($_GET["taille"]))
  {
    $tarif=test_input($_GET["tarif"]);
    $taille=test_input($_GET["taille"]);
  $lesTailles=getPrixByTailleAndTarif($taille,$tarif);
  }
  echo $lesTailles;
  ?>