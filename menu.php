<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand text-warning" href="index.php"><b>GRETA Pizza</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
      aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="index.php">
            <b> Accueil </b> 
            <span class="sr-only">(current)</span></a>
        </li>
        <?php
        if( isset($_SESSION['Id']) )
        {
        echo '<li class="nav-item" style="display:flex; align-items:center;">';
        echo'<a class="nav-link" href="panier.php"><b>Panier</b></a>';
        if( isset($_SESSION['panier']) )
        {
        echo ' <span class="badge badge-primary badge-pill" >'.count($_SESSION['panier']).'</span>';
        }
        echo '</li>';
        echo '<li class="nav-item">';
        echo'<a class="nav-link" href="commandes.php"><b>Vos commandes</b></a>';
        echo '</li>';
        echo '<li class="nav-item">';
        echo'<a class="nav-link" href="identification.php"><b>Deconnexion</b></a>';
        echo '</li>';
       
        }
        else{
        echo '<li class="nav-item">';
        echo '<a class="nav-link" href="identification.php"><b>Connexion</b></a>';
        echo '</li>';
        }
        ?>
       
      </ul>
      <!-- <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  
      </form> -->
    </div>
  </nav>
  <?php
  if( isset($_SESSION['Id']) )
  {
  echo "Bienvenue : ".$_SESSION['Nom'];
  }
  ?>