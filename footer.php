<!-- Footer -->
<footer class="bg-light text-center text-lg-start">
  <!-- Grid container -->
  <div class="container p-4">
    <!--Grid row-->
    <div class="row">
      <!--Grid column-->
      <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
        <h5 class="text-uppercase text-warning"><b>GRETA Pizza</b></h5>
<b>61, rue des Bordes <br>
94430  CHENNEVIERES SUR MARNE</b>
        <p> 
Au-delà d’un restaurant, c’est une histoire d’amour avant tout <br>
Que nous souhaitons partager avec vous ! <br>

Venez déguster LA cuisine authentique italienne <br>
        </p>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Jours</h5>

        <ul class="list-unstyled mb-0">
          <li>
            <a href="#!" class="text-dark">Lundi</a>
          </li>
          <li>
            <a href="#!" class="text-dark">Mardi</a>
          </li>
          <li>
            <a href="#!" class="text-dark">Mercredi</a>
          </li>
          <li>
            <a href="#!" class="text-dark">Jeudi</a>
          </li>
          <li>
            <a href="#!" class="text-dark">Vendredi</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Horaire d'ouverture</h5>

        <ul class="list-unstyled mb-0">
          <li>
            <a href="#!" class="text-dark">9H-17H</a>
          </li>
          <li>
            <a href="#!" class="text-dark">9H-17H</a>
          </li>
          <li>
            <a href="#!" class="text-dark">9H-17H</a>
          </li>
          <li>
            <a href="#!" class="text-dark">9H-17H</a>
          </li>
          <li>
            <a href="#!" class="text-dark">9H-17H</a>
          </li>
        </ul>
      </div>

    </div>
    <!--Grid row-->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
    © 2021 Copyright:
   
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->