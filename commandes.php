
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Greta Pizza</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php
session_start();
include 'menu.php';
include 'php/utilitaire.php';


?>
<div class="container ">
    <div class="row">
        <h1>Vos commandes en cours :</h1>
    </div>
    <div class="row d-flex justify-content-center table-responsive">
   <table class="table table-bordered col-md-10">
   <thead>
    <tr>
    <th scope="col">#</th>
      <th scope="col">Date</th>
      <th scope="col">Type</th>
      <th scope="col">Etat </th>
      <th scope="col">Montant</th>
    </tr>
  </thead>
  <tbody id="commandes">
  </tbody>
   </table>
</div>


<div class="row ">
<h1>Vos commandes précédentes :</h1>
</div>
<div class="row d-flex justify-content-center table-responsive">
<table class="table table-bordered col-md-10 ">
   <thead>
    <tr>
    <th scope="col">#</th>
      <th scope="col">Date</th>
      <th scope="col">Type</th>
      <th scope="col">Etat </th>
      <th scope="col">Montant</th>
    </tr>
  </thead>
  <tbody id="exCommandes">
  </tbody>
   </table>
</div>
</div>
<?php include 'footer.php';?>
<script src="js/fetchCommandes.js"></script>
<?php include 'scripts.php'; ?>
</body>

</html>

